var READTHEDOCS_DATA = {
    project: "tatsu",
    version: "stable",
    language: "en",
    programming_language: "py",
    subprojects: {},
    canonical_url: "https://tatsu.readthedocs.io/en/stable/",
    theme: "alabaster",
    builder: "sphinx",
    docroot: "/docs/",
    source_suffix: ".rst",
    api_host: "https://readthedocs.org",
    proxied_api_host: "https://readthedocs.org",
    commit: "bcfa1838",
    ad_free: false,

    global_analytics_code: "UA-17997319-1",
    user_analytics_code: "UA-37745872-2"
};

